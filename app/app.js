var app = angular.module('app', ['ui.router']);


app.config( function($stateProvider, $urlRouterProvider) {
    var login = {
      name: 'login',
      url: '/',
      templateUrl: 'app/components/session/views/login.html',
      controller:"LoginCtrl"
  }

  var navegador = {
      name: 'navegador',
      url: '/navegador/',
      templateUrl: 'app/components/navegador/views/navegador.html'
      
    }

  var album = {
      name: 'navegador.album',
      url: 'album/All',
      templateUrl: 'app/components/album/views/albumes.html',
      controller:"AlbumCtrl"
   }

  var albumNew = {
    name: 'navegador.albumNew',
    url: 'album/New',
    templateUrl: 'app/components/album/views/formView.html',
    controller:"AlbumCtrl"
    
  }
  var albumEdit = {
    name: 'navegador.albumEdit',
    url: 'album/edit/:id',
    templateUrl: 'app/components/album/views/formView.html',
    controller:"AlbumCtrl"
    
    
  }

   var tracks = {
    name: 'navegador.tracks',
    url: 'track/all',
    templateUrl: 'app/components/track/views/tracks.html',
    controller:"TrackCtrl"
    
    
  }
    var trackNew = {
    name: 'navegador.trackNew',
    url: 'track/new',
    templateUrl: 'app/components/track/views/formView.html',
    controller:"TrackCtrl"
    
    
  }
  
  var trackEdit = {
    name: 'navegador.trackEdit',
    url: 'track/edit/:id',
    templateUrl: 'app/components/track/views/formView.html',
    controller:"TrackCtrl"
    
    
  }
  
   var userNew = {
    name: 'navegador.userNew',
    url: 'user/new',
    templateUrl: 'app/components/usuario/views/formView.html',
    controller: "UserCtrl"
   
  }
  

    var userEdit = {
    name: 'navegador.userEdit',
    url: 'user/edit/:id',
    templateUrl: 'app/components/usuario/views/formView.html',
    controller: "UserCtrl"
    
    
  }

  $stateProvider.state(login);
  $stateProvider.state(navegador);
  $stateProvider.state(album);
  $stateProvider.state(albumNew);
  $stateProvider.state(albumEdit);
  $stateProvider.state(tracks);
  $stateProvider.state(trackNew);
  $stateProvider.state(trackEdit);
  $stateProvider.state(userNew);
  $stateProvider.state(userEdit);
  
  $urlRouterProvider.otherwise('/');
});




app.controller('MainCtrl', ['$scope','$rootScope','$timeout','$http', function ($scope, $rootScope, $timeout, $http){
  console.log("MainCtrl :: ");


  $scope.titulo     = "Main Controller";
  $scope.usuario;


  $rootScope.state  = "login";

  
  $scope.changeState = function (state){
    console.log("state log: ", state);
    $rootScope.state = state;
  }



  $rootScope.audioPlay = "sound/cancion.mp3"; //guarda la ruta de la cancion
  $rootScope.nombreAlbum = "...";
  $rootScope.numeroTrack ="";
  $rootScope.nomTrack = "Seleccione track a escuchar";
  

  

  
  $rootScope.play = false;//identifica si esta en play o pause la musica

  $scope.volumen = 0;//volumen del audio
  $scope.duracion = 225;//duracion total del track actual
  $scope.tiempo = 0;// se relaciona con el valor range del tiempo
  $scope.transcurrido = 0;//variable que guarda la funcion que contabiliza el tiempo transcurrido
  $rootScope.tipoReproduccion = "cancion";// puede ser album o cancion
  $rootScope.ubicacionTrack = 0;

  
  

  
$scope.changePlay = function(){// se encarga del play y pause

    var elemento = document.getElementById('audio');  //guarda la etiqueta de audio en una variable 
        var play = $rootScope.play;//guarda en una variable si la musica esta en play o pause
    if (play == false) {  //Si esta en pausa

      elemento.play(); // le pone play
        $rootScope.play = true; //Cambia la variable a true ya que se hizo play
      document.getElementById('imgPlay').src = "https://img.icons8.com/ios/50/000000/pause-filled.png";//cambia la imagen a pause
      iniciar();    //ejecuta la funcion de conteo
	  
    }else if (play == true){//si esta en play
      elemento.pause();// pone pause
      $rootScope.play = false;// pone la variable en false porque se hizo pause
      document.getElementById('imgPlay').src = "https://img.icons8.com/ios/50/000000/play-filled.png";//cambia la imagen por play
      parar();//Llama a la funcion que detiene el contador
      }
	}
   
     
    $scope.controlVolumen = function(){//controla volumen
        document.getElementById('audio').volume = $scope.volumen / 100;//Toma el valor del range de volumen, lo divide entre 100 para darle el volumen actual
    }
   
  $scope.controlTiempo = function(){//controla tiempo del audio
    document.getElementById('audio').currentTime = $scope.tiempo;//Toma el valor del range del tiempo y lo cambia.
    $scope.cronometro = document.getElementById('audio').currentTime;
  }
   
  function iniciar(){//enciende contador de tiempo y mueve el range
    var indice = $scope.cronometro ;//iguala indice a cronometro
    var dura = $scope.duracion ;//Iguala dura a duracion de la cancion    
    if (indice == null){//Si no esta definido el valor de indice      
      indice = 0; //indice lo iguala a 0    
    }
        indice = indice+1; // le suma 1 a indice
    document.getElementById('valor').innerHTML = indice; //Al campo valor le pasa el valor de indice
    document.getElementById('barraTiempo').value = indice;//A la barra de tiempo lo lleva a el valor de indice
    $scope.cronometro = indice;//A scope.cronometro, le otorga el nuevo valor de indice para seguir sumando
 
        //Utilizando setTimeout enviando una función con parametros
    
    if (indice == (dura +1)){// Se fija si indice supero el valor de la duracion de la cancion
      clearTimeout($scope.transcurrido);//Si es asi elimina el contador
      $scope.cronometro--;//Le resta uno al cronometro para que no supere al maximo
      document.getElementById('valor').innerHTML = (indice - 1);//cambia el contador para que no supere el maximo
      document.getElementById('barraTiempo').value =  (indice - 1);//cambia la barra de tiempo para que no supere el maximo
      siguienteCancion();//como termino la cancion va a la que sigue.
    }else{
      $scope.transcurrido = setTimeout(function(){iniciar(indice)},1000);//Si no supero el tiempo sigue con el proceso de conteo
    }
    }
  
    function parar(){//detiene contador de tiempo y range
    $scope.cronometro--;//Como ya se sumo 1 lo resta en cronometro
    clearTimeout($scope.transcurrido);// Detiene el contadore
    }
	
$rootScope.playTrackSolo = function(){
		

	clearTimeout($scope.transcurrido);

		  

      $rootScope.play = false;//Le decime que esta en pause
      document.getElementById('valor').innerHTML = 0; //Volvemos a 0 el valor del contador que muestra
      document.getElementById('barraTiempo').value = 0;//Dejamos en 0 la barra
      $scope.cronometro = 0; //El cronometro tambien en 0
      $scope.transcurrido = 0;//El tiempo transcurrido tambien en 0
      document.getElementById('imgPlay').src = "https://img.icons8.com/ios/50/000000/play-filled.png";//cambia la imagen por play
      //aca tenemos que poner que pase a la proxima cancion
	  document.getElementById('nombreAlbum').innerHTML = $rootScope.nombreAlbum;
	  document.getElementById('numDelTrack').innerHTML = "";
      document.getElementById('audio').src = $rootScope.audioPlay;
      $scope.transcurrido = setTimeout(function(){
		    $scope.duracion = Math.floor(document.getElementById('audio').duration);
			document.getElementById('tiempoTotal').innerHTML = $scope.duracion;

			$scope.$apply()
		  $scope.changePlay()
	  },500);
	  

		}
		
		
  
  function siguienteCancion(){//se habilita cuando se acaba una cancion
    comprobar = $rootScope.tipoReproduccion;
    if (comprobar == "cancion"){//Si hay una cancion unica 

      $rootScope.play = false;//Le decime que esta en pause
      document.getElementById('valor').innerHTML = 0; //Volvemos a 0 el valor del contador que muestra
      document.getElementById('barraTiempo').value = 0;//Dejamos en 0 la barra
      $scope.cronometro = 0; //El cronometro tambien en 0
      $scope.transcurrido = 0;//El tiempo transcurrido tambien en 0
      document.getElementById('imgPlay').src = "https://img.icons8.com/ios/50/000000/play-filled.png";//cambia la imagen por play
      //Ahora podemos escucharla denuevo.
    }else if(comprobar == "album"){//Si hay un album
	
	dato = $rootScope.listaCompleta;
	ubicacion = $rootScope.ubicacionTrack + 1;
		  
		  if(dato.tracks[ubicacion] == null){
			  		
					
					$rootScope.audioPlay = dato.tracks[0].mp3; 
		$rootScope.nomTrack =dato.tracks[0].name;
		$rootScope.nombreAlbum = dato.nombre;
		$rootScope.playTrackSolo();
		$scope.changePlay();
			}else{
				
		$rootScope.audioPlay = dato.tracks[ubicacion].mp3; 
		$rootScope.nomTrack =dato.tracks[ubicacion].name;
		$rootScope.nombreAlbum = dato.nombre;
		$rootScope.playTrackSolo();

	$rootScope.ubicacionTrack = ubicacion; 
	
	
	
	
    
  
    }}
}
/*
$scope.jojojo = function(){
	$http.get("models/album.json").then(function(datos){
		$scope.equipo = datos;

	});

}
	$scope.jojojo();

	*/


$scope.cambiaTema = function(donde) {//adelanta o atrasa cancion

	if (donde == "delante"){
		comprobar = $rootScope.tipoReproduccion;
		if (comprobar == "cancion"){//Si hay una cancion unica 
			
		}else if(comprobar == "album"){//Si hay un album
		
			dato = $rootScope.listaCompleta;
			ubicacion = $rootScope.ubicacionTrack + 1;
			

			if(dato.tracks[ubicacion] == null){
					
					$rootScope.audioPlay = dato.tracks[0].mp3; 
					$rootScope.nomTrack =dato.tracks[0].name;
					$rootScope.nombreAlbum = dato.nombre;
					$rootScope.playTrackSolo();
					$scope.changePlay();
			}else{
				
					$rootScope.audioPlay = dato.tracks[ubicacion].mp3; 
					$rootScope.nomTrack =dato.tracks[ubicacion].name;
					$rootScope.nombreAlbum = dato.nombre;
					$rootScope.playTrackSolo();
					$rootScope.ubicacionTrack = ubicacion; 
				
			}
		}	
	}else if(donde == "atras"){
		
		dato = $rootScope.listaCompleta;
		
		ubicacion = $rootScope.ubicacionTrack - 1;
		
		if(dato.tracks[ubicacion] == null){
			

		}else{
				
			$rootScope.audioPlay = dato.tracks[ubicacion].mp3; 
			$rootScope.nomTrack =dato.tracks[ubicacion].name;
			$rootScope.nombreAlbum = dato.nombre;
			$rootScope.playTrackSolo();

			$rootScope.ubicacionTrack = ubicacion;
		}
	}
}




  
  

}]);

console.log(app);


